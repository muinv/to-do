import fs from "fs";
import data from "./todos.json" assert { type: "json" };

export const getAllTodos = () => {
  return data;
};

export const addTodo = (todo) => {
  try {
    const todos = [...data, todo];

    fs.writeFileSync("./src/database/todos.json", JSON.stringify(todos));
  } catch (error) {
    console.log(error);
  }
};

export const upTodo = (id) => {
  try {
    const todos = data;
    const index = todos.findIndex((todo) => todo.id === id);
    todos[index].isCompleted = true;

    fs.writeFileSync("./src/database/todos.json", JSON.stringify(todos));
  } catch (error) {
    console.log(error);
  }
};

export const delTodo = (id) => {
  try {
    const todos = data;
    const newTodos = todos.filter((todo) => {
      if (todo.id !== id) {
        return todo;
      }
    });

    fs.writeFileSync("./src/database/todos.json", JSON.stringify(newTodos));
  } catch (error) {
    console.log(error);
  }
};
