import Koa from "koa";
import koaBody from "koa-body";
import { router } from "./routes/routes.js";
import cors from "@koa/cors";

const app = new Koa();

app.use(cors());
app.use(koaBody());
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(5000);
