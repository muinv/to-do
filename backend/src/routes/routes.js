import Router from "koa-router";

import { todoInputMiddleware } from "../middleware/todoInputMiddleware.js";
import {
  getTodos,
  createTodo,
  updateTodo,
  deleteTodo,
} from "../handlers/todos/todoHandlers.js";

// Prefix all routes with /todos
const router = new Router({
  prefix: "/api",
});

// Routes will go here
router.get("/todos", getTodos);
router.post("/todos", todoInputMiddleware, createTodo);
router.put("/todos/:id", updateTodo);
router.delete("/todos/:id", deleteTodo);

export { router };
