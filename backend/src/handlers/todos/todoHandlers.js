import {
  getAllTodos,
  addTodo,
  upTodo,
  delTodo,
} from "../../database/todoRepository.js";

export const getTodos = (ctx) => {
  try {
    const todos = getAllTodos();

    ctx.body = {
      data: todos,
    };
  } catch (error) {
    ctx.status = 500;
    ctx.body = {
      success: false,
      data: [],
      error: error.message,
    };
  }
};

export const createTodo = (ctx) => {
  try {
    const postData = ctx.request.body;
    addTodo(postData);

    ctx.status = 201;
    return (ctx.body = {
      success: true,
      data: postData,
    });
  } catch (error) {
    ctx.status = 500;
    ctx.body = {
      success: false,
      error: error.message,
    };
  }
};

export const updateTodo = (ctx) => {
  try {
    const id = parseInt(ctx.params.id);
    upTodo(id);

    ctx.status = 200;
    return (ctx.body = {
      success: true,
    });
  } catch (error) {
    ctx.status = 500;
    return (ctx.body = {
      success: false,
      error: error.message,
    });
  }
};

export const deleteTodo = (ctx) => {
  try {
    const id = parseInt(ctx.params.id);
    delTodo(id);

    ctx.status = 204;
    return (ctx.body = null);
  } catch (error) {
    ctx.status = 500;
    return (ctx.body = {
      success: false,
      error: error.message,
    });
  }
};
