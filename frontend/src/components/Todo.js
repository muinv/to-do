export default function Todo({ todo, index, completeTodo, removeTodo, id }) {
  return (
    <div
      className="todo"
      style={{ textDecoration: todo.isCompleted ? "line-through" : "" }}
    >
      {todo.task}
      <div>
        <button onClick={() => completeTodo(index, id)}>Complete</button>
        <button onClick={() => removeTodo(index, id)}>x</button>
      </div>
    </div>
  );
}
