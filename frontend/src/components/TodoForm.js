import React from "react";

export default function TodoForm({ id, addTodo }) {
  const [value, setValue] = React.useState({
    id: id,
    task: "",
    isCompleted: false,
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!value.task) return;
    addTodo(value);
    setValue({ task: "" });
  };

  const handleChange = (e) => {
    setValue({
      id: id,
      task: e.target.value,
      isCompleted: false,
    });
  };

  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        className="input"
        value={value.task}
        onChange={handleChange}
      />
    </form>
  );
}
