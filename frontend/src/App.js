import React, { useEffect } from "react";
import "./App.css";
import TodoForm from "./components/TodoForm";
import Todo from "./components/Todo";
import { useFetchData } from "./hooks/useFetchData";
import { deleteData, postData, updateData } from "./service";

function App() {
  const { data } = useFetchData("http://localhost:5000/api/todos");
  const [todos, setTodos] = React.useState([]);

  useEffect(() => {
    setTodos(data);
  }, [data]);

  const addTodo = async (data) => {
    await postData("http://localhost:5000/api/todos", data);
    const newTodos = [...todos, data];
    setTodos(newTodos);
  };

  const completeTodo = async (index, id) => {
    await updateData(`http://localhost:5000/api/todos/${id}`);
    const newTodos = [...todos];
    newTodos[index].isCompleted = true;
    setTodos(newTodos);
  };

  const removeTodo = async (index, id) => {
    await deleteData(`http://localhost:5000/api/todos/${id}`);
    const newTodos = [...todos];
    newTodos.splice(index, 1);
    setTodos(newTodos);
  };

  return (
    <div className="app">
      <div className="todo-list">
        {todos.map((todo, index) => (
          <Todo
            key={index}
            index={index}
            todo={todo}
            completeTodo={completeTodo}
            removeTodo={removeTodo}
            id={todo.id}
          />
        ))}
        <TodoForm id={new Date().getTime()} addTodo={addTodo} />
      </div>
    </div>
  );
}

export default App;
