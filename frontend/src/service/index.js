export const postData = async (url, data) => {
  try {
    await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });
  } catch (error) {
    console.log(error);
  }
};

export const updateData = async (url) => {
  try {
    await fetch(url, {
      method: "PUT",
    });
  } catch (error) {
    console.log(error);
  }
};

export const deleteData = async (url) => {
  try {
    await fetch(url, {
      method: "DELETE",
    });
  } catch (error) {
    console.log(error);
  }
};
